This guide will let you know how you can contribute to the project.

# Branches

Here are how the different branches interact together:

 - master: contains the current release of the project
 - dev: contains the worked-on release (the next one)
 - api: contains changes to the public API
 - json: contains changes to the JSON integration
 - xml: contains changes to the XML integration
 - ...: other branches are created per-feature.

# Pull requests

In which branch should you make your pull request?

First, we will not accept ANY pull request on the master branch.
The master branch is used to store the current release of the project
(for example so the non-Gradle or non-Maven users can still use the project).
Therefore, we will close any pull request there.

You can, however, create pull-requests to:

 - dev: for project-wide changes (gradle configuration...)
 - api: for public API changes (mainly the 'resourcer' package)
 - json: for changes regarding the handling of JSON (mainly the 'json' package)
 - xml: for changes regarding the handling of XML (mainly the 'xml' package)

In most cases, other branches will be created per-feature, so it
doesn't make sense to submit a PR there - however, we do not forbid it
(if you're convinced its the best place to submit your PR, go ahead).
